#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size() <= 1){
        return;
    }
	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    for (int i = 0; i < first.size();i++){
            first.insert(i,origin[i]);
    }
    for(int j = 0; j < second.size(); j++){
            second.insert(j,origin[j+first.size()]);
    }
	// recursiv splitAndMerge of lowerArray and greaterArray
    splitAndMerge(first);
    splitAndMerge(second);
	// merge
    merge(first,second,origin);
}

void merge(Array& first, Array& second, Array& result)
{
    int i = 0,j = 0, k = 0;
    for(; i < result.size(); i++){
        if(j < first.size() && k < second.size()){
            if(first[j] < second[k]){
                result[i] = first[j];
                j++;
            }
            else {
                result[i] = second[k];
                k++;
            }
        }
        else if(j >= first.size()){
            result[i] = second[k];
            k++;
        }
        else if(k >= second.size()){
            result[i] = first[j];
            j++;
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
