#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    if(n>0){
        float carreX = z.x*z.x;
        float carreY = z.y*z.y;
        float mod = sqrt(carreX+carreY);
        if(mod > 2){
            return false;
        }
        else {
            float reel = z.x;
            float img = z.y;
            float reelA = point.x;
            float imgA = point.y;
            Point suite;
            suite.x = reel*reel - img*img +reelA;
            suite.y = 2*reel*img+imgA;
            isMandelbrot(suite,n-1,point);
        }
    }
    else {
        return true;
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



