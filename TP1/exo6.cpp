#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
};

struct DynaTableau{
    int* donnees;
    int capacite;
    int size;
};


void initialise(Liste* liste)
{
    liste->premier = NULL;
}

bool est_vide(const Liste* liste)
{
    bool res = true;
    if(liste->premier){
        res = false;
    }
    return res;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* n1 = (Noeud*)malloc(sizeof (*n1));
    n1->donnee = valeur;
    n1->suivant = NULL;
    if(!liste->premier){
        liste->premier = n1;
    }
    else {
        Noeud* fin = liste->premier;
        while(fin->suivant != NULL){
            fin = fin->suivant;
        }
        fin->suivant = n1;
    }
}

void affiche(const Liste* liste)
{
    Noeud* fin = liste->premier;
    while(fin != NULL){
        cout << fin->donnee << " ";
        fin = fin->suivant;
    }
    cout << endl;
}

int recupere(const Liste* liste, int n)
{
    int res = 0;
    Noeud* fin = liste->premier;
    for(int i = 0; i < n; i++){
        fin = fin->suivant;
    }
    if(fin->donnee){
        res = fin->donnee;
    }
    return res;
}

int cherche(const Liste* liste, int valeur)
{
    int index = 0;
    Noeud* fin = liste->premier;
    while(fin != NULL){
        if(fin->donnee == valeur){
            return index;
        }
        fin = fin->suivant;
        index++;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* fin = liste->premier;
    for(int i = 0; i < n; i++){
        fin = fin->suivant;
    }
    if(fin->donnee){
        fin->donnee = valeur;
    }
    else {
        cout << "Il n'y a pas d'entier à remplacer" << endl;
    }
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->donnees = (int*) malloc((tableau->capacite)*sizeof (int));
    if (!tableau->donnees){
        cout << "L'allocation a echouer." << endl;
        exit(1);
    }
    tableau->size = 0;
}

bool est_vide(const DynaTableau* tableau)
{
    bool res = true;
    if(tableau->size){
        res = false;
    }
    return res;
}

void affiche(const DynaTableau* tableau)
{
    for(int i = 0; i < tableau->size; i++){
        cout << tableau->donnees[i] << " ";
    }
    cout << endl;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    tableau->size++;
    if(tableau->size > tableau->capacite){
        tableau->capacite++;
        tableau->donnees = (int*)realloc(tableau->donnees,(tableau->capacite)*sizeof(int));
        if(tableau->donnees == NULL){
            exit(1);
        }
    }
    tableau->donnees[tableau->size-1] = valeur;
}

int recupere(const DynaTableau* tableau, int n)
{
    int res = tableau->donnees[n];
    return res;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    int index = -1;
    for(int i = 0; i < tableau->size; i++){
        if(tableau->donnees[i] == valeur){
            index = i;
            break;
        }
    }
    return index;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n] = valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    Noeud* n1 = (Noeud*)malloc(sizeof (*n1));
    n1->donnee = valeur;
    n1->suivant = NULL;
    if(!(liste->premier)){
        liste->premier = n1;
    }
    else {
        Noeud *fin = liste->premier;
        while(fin->suivant != NULL){
            fin = fin->suivant;
        }
        fin->suivant = n1;
    }
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if(liste == NULL){
        exit(1);
    }
    int valFile = 0;
    if(liste->premier != NULL){
        Noeud *prems = liste->premier;
        valFile = prems->donnee;
        liste->premier = prems->suivant;
        free(prems);
    }
    return valFile;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud* n1 = (Noeud*)malloc(sizeof (*n1));
    n1->donnee = valeur;
    n1->suivant = liste->premier;
    liste->premier = n1;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if(liste == NULL){
        exit(1);
    }
    int valPile = 0;
    Noeud *prems = liste->premier;
    if(liste->premier != NULL && liste != NULL){
        valPile = prems->donnee;
        liste->premier = prems->suivant;
        free(prems);
    }
    return valPile;
}

int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }
    affiche(&pile);
    affiche(&file);

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
