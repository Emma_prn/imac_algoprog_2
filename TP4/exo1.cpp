#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    int i = heapSize;
    (*this)[i] = value;
    while(i>0 && (*this)[i] > (*this)[(i-1)/2]){
        this->swap(i,(i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    int i_max = nodeIndex;
    int left = this->leftChild(i_max);
    int right = this->rightChild(i_max);
    int largest = i_max;
    if(left < heapSize && (*this)[left] > (*this)[largest]){
        largest = left;
    }
    if(right < heapSize && (*this)[right] > (*this)[largest]){
        largest = right;
    }
    if(largest != i_max){
        this->swap(i_max,largest);
        this->heapify(heapSize,largest);
    }

}

void Heap::buildHeap(Array& numbers)
{
    int start = (numbers.size()/2)-1;
    for(int i = start; i >= 0; i--){
        this->heapify(numbers.size(),i);
    }
}

void Heap::heapSort()
{
    for(int i = this->size()-1;i > 0;i--){
        this->swap(0,i);
        this->heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
